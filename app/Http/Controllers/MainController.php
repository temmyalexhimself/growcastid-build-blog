<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $posts = Post::where('status', 'active')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();

        return view('index', ['posts' => $posts]);
    }
}
