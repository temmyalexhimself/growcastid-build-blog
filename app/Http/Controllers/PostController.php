<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();
        // $posts = Post::where('user_id', Auth::user()->id)->get();
        return view('backend.posts.index', ['posts' => $posts]);
    }

    public function create()
    {
        return view('backend.posts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'status' => 'required'
        ]);

        Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'slug' => Str::slug($request->title, '-'),
            'status' => $request->status,
            'user_id' => $request->user()->id
        ]);

        return redirect()->route('posts.index')->with('success', 'Success!');
    }

    public function edit($id)
    {
        // SELECT * FROM posts WHERE id = $id;
        $post = Post::find($id);
        $this->authorize('update-post', $post);
        return view('backend.posts.edit', ['post' => $post]);
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $this->authorize('update-post', $post);
        $post->update([
            'title' => $request->title,
            'description' => $request->description,
            'status' => $request->status,
            'slug' => Str::slug($request->title, '-'),
            'user_id' => $request->user()->id
        ]);

        return redirect()->route('posts.index')->with('success', 'Success!');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $this->authorize('delete-post', $post);
        $post->delete();

        return redirect()->route('posts.index')->with('success', 'Success!');
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;

            $request->file('upload')->move(public_path('images'), $fileName);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/' . $fileName);
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; chartset=utf-8');
            echo $response;
        }
    }

    public function list()
    {
        $posts = Post::where('status', 'active')
                ->orderBy('created_at', 'desc')
                ->simplePaginate(6);
        return view('frontend.list', ['posts' => $posts]);
    }

    public function detail($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('frontend.detail', ['post' => $post]);
    }
}
