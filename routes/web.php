<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index']);

Route::group(['prefix' => 'cms'], function(){
    Auth::routes([
        'register' => false,
        'reset' => false,
        'verify' => false
    ]);
});

// Middleware user hanya boleh akses halaman ketika login
Route::group(['middleware' => 'auth'], function(){
    Route::get('users/edit-profile', [UserController::class, 'edit_profile'])->name('users.edit-profile');
    Route::patch('users/update-profile', [UserController::class, 'update_profile'])->name('users.update-profile');
    Route::resource('users', UserController::class);

    Route::get('posts/index', [PostController::class, 'index'])->name('posts.index');
    Route::get('posts/create', [PostController::class, 'create'])->name('posts.create');
    Route::post('posts/store', [PostController::class, 'store'])->name('posts.store');
    Route::post('post/upload', [PostController::class, 'upload'])->name('post.upload');
    Route::get('posts/edit/{id}', [PostController::class, 'edit'])->name('posts.edit');
    Route::patch('posts/update/{id}', [PostController::class, 'update'])->name('posts.update');
    Route::delete('posts/delete/{id}', [PostController::class, 'destroy'])->name('posts.destroy');
});

Route::get('articles', [PostController::class, 'list'])->name('articles.list');
Route::get('articles/{slug}', [PostController::class, 'detail'])->name('articles.detail');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
