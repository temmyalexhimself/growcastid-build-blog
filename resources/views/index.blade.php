@extends('layouts.frontend.master')

@section('title', 'Home')

@section('header')
<!-- Page Header-->
<header class="masthead" style="background-image: url('{{ asset('frontend/assets/img/home-bg.jpg') }}')">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <div class="site-heading">
                    <h1>Clean Blog</h1>
                    <span class="subheading">A Blog Theme by Start Bootstrap</span>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
<div class="row gx-4 gx-lg-5 justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
        @forelse ($posts as $post)
            <!-- Post preview-->
            <div class="post-preview">
                <a href="#">
                    <h2 class="post-title">{{ $post->title }}</h2>
                    <h3 class="post-subtitle">{!! substr($post->description,0,100) !!}...</h3>
                </a>
                <p class="post-meta">
                    Posted by
                    <a href="#!">{{ $post->user->name }}</a>
                    {{-- on September 24, 2021 --}}
                    {{ $post->getDate() }}
                </p>
            </div>
            <hr class="my-4" />
        @empty
            <h1>No Posts</h1>
        @endforelse
        <!-- Divider-->
    </div>
</div>
@endsection
