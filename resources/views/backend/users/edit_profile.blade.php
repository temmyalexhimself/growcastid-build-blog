@extends('layouts.backend.dashboard.master')

@section('title', 'Edit Profile')

@section('content')
<div class="container">
    @include('layouts.backend.dashboard.include.message')
    <div class="card shadow">
        <div class="card-header">Edit Profile</div>
        <div class="card-body">
            <form action="{{ route('users.update-profile') }}" method="POST">
                @csrf
                @method('PATCH')

                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" class="form-control @error('name') is-invalid @enderror" type="text" name="name"
                        value="{{ old('name', $user->name) }}">

                    @error('name')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" class="form-control @error('email') is-invalid @enderror" type="text" name="email"
                        value="{{ old('email', $user->email) }}">

                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password">

                    @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Password Confirmation</label>
                    <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" name="password_confirmation">

                    @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
