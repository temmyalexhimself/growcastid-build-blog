@extends('layouts.backend.dashboard.master')

@section('title', 'List Users')

@section('content')
<div class="container">
    @include('layouts.backend.dashboard.include.message')
    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="{{ route('users.create') }}" class="btn btn-success mb-4">Create</a>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped" id="table-users">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                             <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">Edit</a>
                        </td>
                     </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready( function () {
            $('#table-users').DataTable();
        });
    </script>
@endpush
