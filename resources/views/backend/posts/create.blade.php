@extends('layouts.backend.dashboard.master')

@section('title', 'Article Create')

@section('content')
<div class="container">
    <div class="card shadow">
        <div class="card-header">Create Article</div>
        <div class="card-body">
            <form action="{{ route('posts.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Title</label>
                    <input id="title" class="form-control @error('title') is-invalid @enderror" type="text" name="title"
                        value="{{ old('title') }}">

                    @error('title')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="my-textarea">Description</label>
                    <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description">{{ old('description') }}</textarea>

                    @error('description')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="role">Status</label>
                    <select id="status" class="form-control @error('status') is-invalid @enderror" name="status">
                        <option value="">-- Select Status --</option>
                        <option value="active" {{ old('status') == 'active' ? 'selected' : '' }}>Active</option>
                        <option value="inactive" {{ old('status') == 'inactive' ? 'selected' : '' }}>Inactive</option>
                    </select>

                    @error('status')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="" class="btn btn-success">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script>
        // Tanpa upload gambar
        // CKEDITOR.replace('description');

        // Menggunakan fungsi upload gambar
        CKEDITOR.replace('description', {
            filebrowserUploadUrl: "{{ route('post.upload', ['_token' => csrf_token() ]) }}",
            filebrowserUploadMethod: 'form',
            disallowedContent: 'img{width,height};'
        });
    </script>
@endpush
