@extends('layouts.backend.dashboard.master')

@section('title', 'Article Edit')

@section('content')
<div class="container">
    <div class="card shadow">
        <div class="card-header">Edit Article</div>
        <div class="card-body">
            <form action="{{ route('posts.update', $post->id) }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Title</label>
                    <input id="title" class="form-control @error('title') is-invalid @enderror" type="text" name="title"
                        value="{{ old('title', $post->title) }}">

                    @error('title')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="my-textarea">Description</label>
                    <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description">{{ old('description', $post->description) }}</textarea>

                    @error('description')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="role">Status</label>
                    <select id="status" class="form-control @error('status') is-invalid @enderror" name="status">
                        <option value="">-- Select Status --</option>
                        <option value="active" {{ old('status') == 'active' ? 'selected' : '' }}
                            {{ $post->status == 'active' ? 'selected' : '' }}>
                            Active
                        </option>
                        <option value="inactive" {{ old('status') == 'inactive' ? 'selected' : '' }}
                            {{ $post->status == 'inactive' ? 'selected' : '' }}>
                            Inactive
                        </option>
                    </select>

                    @error('status')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="" class="btn btn-success">Back</a>
            </form>
        </div>
    </div>
</div>
@endsection
