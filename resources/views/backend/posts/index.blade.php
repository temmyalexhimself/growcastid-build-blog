@extends('layouts.backend.dashboard.master')

@section('title', 'List Articles')

@section('content')
<div class="container">
    @include('layouts.backend.dashboard.include.message')
    <div class="row">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="{{ route('posts.create') }}" class="btn btn-success mb-4">Create</a>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-striped" id="table-posts">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $post->title }}</td>
                            <td>
                                <span class="badge badge-{{ $post->status == 'active' ? 'success' : 'danger' }}">
                                    {{ $post->status == 'active' ? 'Active' : 'Inactive' }}
                                </span>
                            </td>
                            <td>
                                @can('update-post', $post)
                                    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-success">Edit</a>
                                @endcan

                                @can('delete-post', $post)
                                    <button id="delete" data-title="{{ $post->title }}"
                                        href="{{ route('posts.destroy', $post->id) }}" class="btn btn-danger btn-sm d-inline">
                                        <i class="zmdi zmdi-delete zmdi-hc-fw" style="color: #fff; font-size: 1.5em;"></i>
                                        Delete
                                    </button>

                                    <form method="post" id="deleteForm">
                                        @csrf
                                        @method("DELETE")
                                        <input type="submit" value="" style="display:none;">
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#table-posts').DataTable();
        });

        $('button#delete').on('click', function(){
            var href = $(this).attr('href');
            var title = $(this).data('title');

            swal({
                title: "Apakah Anda yakin ingin menghapus "+ title,
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    document.getElementById('deleteForm').action = href;
                    document.getElementById('deleteForm').submit();
                    swal("Artikel berhasil dihapus!", {
                        icon: "success",
                    });
                }
            });
        });
    </script>
@endpush
